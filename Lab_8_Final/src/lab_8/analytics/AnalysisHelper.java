/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab_8.analytics;

import static java.lang.Math.E;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import lab_8.entities.Comment;
import lab_8.entities.Post;
import lab_8.entities.User;
import java.util.Iterator;

class ValueComparator<T> implements Comparator<T> {

    Map<T, Integer> base;

    public ValueComparator(Map<T, Integer> base) {
        this.base = base;
    }

    @Override
    public int compare(T a, T b) {
        if (base.get(a) >= base.get(b)) {
            return -1;
        } else {
            return 1;
        } // returning 0 would merge keys
    }
}

/**
 *
 * @author harshalneelkamal
 */
public class AnalysisHelper {

    public void userWithMostLikes() {
        Map<Integer, Integer> userLikecount = new HashMap<Integer, Integer>(); // ask about this step.
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        for (User user : users.values()) {
            for (Comment c : user.getComments()) {
                int likes = 0;
                if (userLikecount.containsKey(user.getId())) {
                    likes = userLikecount.get(user.getId());
                    System.out.println(likes);
                    //System.out.println("yes userlikecount has user" );
                }
                likes += c.getLikes();
                userLikecount.put(user.getId(), likes);

            }

        }

        int max = 0;
        int maxId = 0;
        for (int id : userLikecount.keySet()) {
            if (userLikecount.get(id) > max) {
                max = userLikecount.get(id);
                maxId = id;
            }

        }

        System.out.println("User with most likes: " + max + "\n" + users.get(maxId));

    }

    public void getFiveMostLikedComment() {
        Map<Integer, Comment> commnets = DataStore.getInstance().getComments();
        List<Comment> commentList = new ArrayList<>(commnets.values());
        Collections.sort(commentList, new Comparator<Comment>() {

            @Override
            public int compare(Comment o1, Comment o2) {
                return o2.getLikes() - o1.getLikes();
                //throw new UnsupportedOperationException("Not supported yet."); 
                //To change body of generated methods, choose Tools | Templates.
            }

        });
        System.out.println("5 most liked coments: ");
        for (int i = 0; i < commentList.size() && i < 5; i++) {
            System.out.println(commentList.get(i));

        }

    }

    public void getPostWithMostLikedComment() {
        Map<Integer, Integer> PostCommentLikecount = new HashMap<Integer, Integer>();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        for (Post post : posts.values()) {
            //System.out.println("inside post loop: " + post.getPostId());
            int likes = 0;
            for (Comment c : post.getComments()) {
                //System.out.println("inside comment loop with comment Id: " + c.getId() + " and likes " + c.getLikes());
                likes += c.getLikes();
            }
            //System.out.println("post id: " + post.getPostId() + " having total likes on all posts " + likes);
            PostCommentLikecount.put(post.getPostId(), likes);
        }
        //Calculating the max likes over the post comments for a particular post
        int maxLikes = 0;
        int MaxLikesPostId = 0;
        for (int id : PostCommentLikecount.keySet()) {
            if (PostCommentLikecount.get(id) > maxLikes) {
                maxLikes = PostCommentLikecount.get(id);
                MaxLikesPostId = id;
            }
        }
        System.out.println("********** PART 2 - Post with most liked comments **************\n");
        System.out.println("PostId with most Liked Comments: " + MaxLikesPostId + " having total likes " + maxLikes + "\n");

    }

    public void getTopFiveProactiveUsers() {

        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        Map<Integer, Integer> NumberOfPosts = new HashMap<>();
        Map<Integer, Integer> NumberOfComments = new HashMap<>();
        Map<Integer, Integer> TotalProactiveness = new HashMap<>();
        int proactiveness = 0;
        for (User u : users.values()) {
            int i = 0;
            for (Post p : posts.values()) {
                if (u.getId() == p.getUserId()) {
                    i++;
                }
            }
            NumberOfPosts.put(u.getId(), i);
            //System.out.println("user id: " + u.getId() + " number of posts " + i);
        }
        for (User u : users.values()) {
            int i = 0;
            for (Comment c : comments.values()) {
                if (u.getId() == c.getUserId()) {
                    i++;
                }
            }
            NumberOfComments.put(u.getId(), i);
            //System.out.println("user id: " + u.getId() + " number of comments " + i);
        }
        for (int user : NumberOfPosts.keySet()) {
            proactiveness = NumberOfPosts.get(user) + NumberOfComments.get(user);
            TotalProactiveness.put(user, proactiveness);
            //System.out.println("user id: " + user + "  total " + proactiveness);
        }
        System.out.println("\n********PART 7 - Top 5 Proactive users based on total comments and posts*****\n");
        printTopUsers(TotalProactiveness, 5);
    }

    public static void printTopUsers(Map m, int top) {
        int count = 0;
        Map<Integer, Integer> TopUsers = new HashMap<>();
        TopUsers = sortByValue(m);
        Iterator<Entry<Integer, Integer>> iter = TopUsers.entrySet().iterator();
            while (iter.hasNext()) {
                count++;
                Entry<Integer, Integer> entry = iter.next();
                System.out.println("User Id: " + entry.getKey() + "  Proactiveness (Posts + Comments): " + entry.getValue());
                if(top == count){
                    break;
                }
            }
    }

    public static Map sortByValue(Map unsortedMap) { 
        Map sortedMap = new TreeMap(new ValueComparator(unsortedMap));
        sortedMap.putAll(unsortedMap);
        return sortedMap;
    }

    public void getPostWithMostComments() {
        Map<Integer, Post> post = DataStore.getInstance().getPosts();
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        List<Post> postList = new ArrayList<>(post.values());
        for (Post p : postList) {
            for (Comment c : comments.values()) {
                if (p.getPostId() == c.getPostId()) {
                    Collections.sort(postList, new Comparator<Post>() {
                        @Override
                        public int compare(Post p1, Post p2) {
                            return p2.getComments().size() - p1.getComments().size();
                        }
                    });
                }
            }
        }
        System.out.println("********** PART 3 - Post with most comments **************\n");
        for (int i = 0; i < 1; i++) {
            Post posts = postList.get(i);
            System.out.println("Post with most comments:" + postList.get(i) + "\n");

        }
    }

    public void getUserWithLeastComments() {
        Map<Integer, User> userCommentCount = DataStore.getInstance().getUsers();
        List<User> userList = new ArrayList<>(userCommentCount.values());
        Collections.sort(userList, new Comparator<User>() {
            @Override
            public int compare(User u1, User u2) {
                return u1.getComments().size() - u2.getComments().size();

            }
        });
        System.out.println("\n*************PART 5 - Top 5 inactive users based on comments***********\n");
        for (int i = 0; i < userList.size() && i < 5; i++) {
            User user = userList.get(i);
            System.out.println("Top 5 Inactive Users based on comments:" + userList.get(i));

        }
    }

    public void getUserWithLeastPost() {
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        Map<Integer, Integer> userPosts = new HashMap<>();
        for (User user : users.values()) {
            int i = 0;
            for (Post post : posts.values()) {

                if (post.getUserId() == user.getId()) {
                    i++;
                }
            }
            userPosts.put(user.getId(), i);
        }
        Set<Entry<Integer, Integer>> set = userPosts.entrySet();
        List<Entry<Integer, Integer>> list = new ArrayList<Entry<Integer, Integer>>(set);
        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> o1,
                    Map.Entry<Integer, Integer> o2) {
                return o1.getValue().compareTo(o2.getValue());
            }
        });
        System.out.println("\n***********PART 4 - Top 5 Inactive Users based on post*************\n");
        for (int i = 0; i < list.size() && i < 5; i++) {
            Entry user1 = list.get(i);
            System.out.println("User Id with least post:" + user1.getKey() + " " + "The number of posts are:" + user1.getValue());
        }
    }

    public void getTopFiveInactiveUsers() {
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        Map<Integer, Integer> commentsCountPerUser = new HashMap<>(); //userId,noOfCommentsByUserID
        Map<Integer, Integer> postsCountPerUser = new HashMap<>(); //userId,noOfPostsByUserID
        Map<Integer, Integer> totalPostsNComments = new HashMap<>(); //userID,totalNoOfPostsAndComments

        //Find no of comments made by each user, store it in hashmap "commentsCountPerUser"
        for (User user : users.values()) {
            //System.out.println(user.getComments());
            //System.out.println(user.getComments().size()+"\n");
            commentsCountPerUser.put(user.getId(), user.getComments().size());
        }
        //System.out.println("\nUserId, No of comments by UserID: \n" + commentsCountPerUser);

        //Find no of posts made by each user, store it in hashmap "postsCountPerUser"
        for (User user : users.values()) {
            int no_of_posts = 0;
            for (Post post : posts.values()) {
                if (user.getId() == post.getUserId()) {
                    no_of_posts++;
                }
            }
            //System.out.println(user.getId()+" has "+i+" number of posts.");
            postsCountPerUser.put(user.getId(), no_of_posts);
        }
        //System.out.println("UserId, No of posts by UserID: \n" + postsCountPerUser);

        //To find total of comments and posts by a single userID
        //int total = 0;
        for (User user : users.values()) {
            int comment_count = (commentsCountPerUser.get(user.getId()) == null) ? 0 : commentsCountPerUser.get(user.getId());
            int posts_count = (postsCountPerUser.get(user.getId()) == null) ? 0 : postsCountPerUser.get(user.getId());
            int total = comment_count + posts_count;
            totalPostsNComments.put(user.getId(), total);
        }
        //System.out.println("UserId, Total of comments and posts: \n" + totalPostsNComments);

        //To find top 5 inactive users bases on least number of comments and posts
        List<Entry<Integer, Integer>> proactiveList = new ArrayList<Map.Entry<Integer, Integer>>(totalPostsNComments.entrySet());
        Comparator<Map.Entry<Integer, Integer>> com = new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
                return (o1.getValue().compareTo(o2.getValue()));
                //throw new UnsupportedOperationException("Not supported yet."); 
                //To change body of generated methods, choose Tools | Templates.
            }
        };
        Collections.sort(proactiveList, com); //sort in ascending order. (Least comments and posts first.)
        //System.out.println("Sorted proactiveness in asc order: \n" + proactiveList);

        int temp = 0;
        System.out.println("\n********PART 6 - Top 5 inactive users based on total comments and posts*****\n");
        for (Entry<Integer, Integer> entry : proactiveList) {
            //System.out.println(entry);
            System.out.println("User Id: " + entry.getKey() + "  Proactiveness (Posts + Comments): "+ entry.getValue());
            temp++;
            if (temp == 5) {
                break;
            }
        }
    }

    public void getAverageLikesPerComment() {
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        double total_no_of_comments = 0;
        double total_no_of_likes = 0;
        for (Integer key : comments.keySet()) {
            total_no_of_comments++;
            total_no_of_likes += comments.get(key).getLikes();
        }
        System.out.println("********** PART 1 - Average No. Of likes Per comment **************");
        System.out.println("\nTotal no of comments: " + total_no_of_comments);
        System.out.println("Total no of likes in all: " + total_no_of_likes);
        try {
            double averageLikes = total_no_of_likes / total_no_of_comments;
            System.out.println("Average number of likes per comment: " + averageLikes + "\n");

        } catch (ArithmeticException ar) {
            System.out.println("Arithmatic Exception occurred.");
            //ar.printStackTrace();
        }
    }
}
