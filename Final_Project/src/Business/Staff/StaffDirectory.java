/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Staff;

import java.util.ArrayList;

/**
 *
 * @author divinity
 */
public class StaffDirectory {
    
        private ArrayList<Staff> staffList;

    public StaffDirectory() {
        staffList = new ArrayList();
    }

    public ArrayList<Staff> getStaffList() {
        return staffList;
    }


    
    public Staff createStaff(String name){
        Staff stf = new Staff();
        stf.setName(name);
        staffList.add(stf);
        return stf;
    }
    
}
