/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.HCPEmployee;

import Business.Department.Department;

/**
 *
 * @author divinity
 */
public class Nurse {
    
    private String nurseRegNumber;
    private String nurseName;
    private Department dep;
    private int id = 8000;


    public Nurse(Department dep) {
        this.dep = dep;
        id++;
        nurseRegNumber = "Nurse_"+dep.getDepName()+id;
    }

    public String getNurseRegNumber() {
        return nurseRegNumber;
    }

    public String getNurseName() {
        return nurseName;
    }

    public void setNurseName(String nurseName) {
        this.nurseName = nurseName;
    }

    
    
    
}
