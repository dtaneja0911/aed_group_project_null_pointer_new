/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.HCPEmployee;

import Business.Department.Department;

/**
 *
 * @author divinity
 */
public class Doctor {
    
    private String doctorRegNumber;
    private String doctorName;
    private Department dep;
    private int id = 5000;

    public String getDoctorRegNumber() {
        return doctorRegNumber;
    }
    
    public Doctor(Department dep) {
        this.dep = dep;
        id++;
        doctorRegNumber = "Doc_"+dep.getDepName()+id;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }
    
    
}
