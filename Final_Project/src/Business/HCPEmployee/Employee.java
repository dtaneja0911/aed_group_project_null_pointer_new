  /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.HCPEmployee;

import Business.Department.Department;

/**
 *
 * @author divinity
 */
public class Employee {
    
    private DoctorDirectory doctorDir;
    private LabAssistantDirectory labAssistantDir;
    private NurseDirectory nursesDir;
    private Department dep;

    public Employee(Department dep) {
        
        doctorDir = new DoctorDirectory();
        labAssistantDir = new LabAssistantDirectory();
        nursesDir = new NurseDirectory();
        this.dep = dep;
   
    }

    public DoctorDirectory getDoctorDir() {
        return doctorDir;
    }

    public LabAssistantDirectory getLabAssistantDir() {
        return labAssistantDir;
    }

    public NurseDirectory getNursesDir() {
        return nursesDir;
    }

    public Department getDep() {
        return dep;
    }
    
    
    
    
}