/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.HCPEmployee;

import Business.Department.Department;

/**
 *
 * @author divinity
 */
public class LabAssistant {

    private String labAssistantRegNumber;
    private String labAssistantName;
    private Department dep;
    private int id = 9000;

    public LabAssistant(Department dep) {
        this.dep = dep;
        id++;
        labAssistantRegNumber = "LabAssistant_" + dep.getDepName() + id;
    }

    public String getLabAssistantRegNumber() {
        return labAssistantRegNumber;
    }

    public String getLabAssistantName() {
        return labAssistantName;
    }

    public void setLabAssistantName(String labAssistantName) {
        this.labAssistantName = labAssistantName;
    }

}
