/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Manufacturer;

import java.util.ArrayList;

/**
 *
 * @author divinity
 */
public class ManufacturerDirectory {

    private ArrayList<Manufacturer> manuList;

    public ManufacturerDirectory() {
        manuList = new ArrayList<>();

    }

    public ArrayList<Manufacturer> getManuList() {
        return manuList;
    }
    
    
      public Manufacturer createAndAddManufacturer(String name) {
        Manufacturer manu1 = new Manufacturer(name );
        manuList.add(manu1);
        return manu1;
    }
    
}
