/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Manufacturer;

import Business.DeviceCategory.DeviceDirectory;
import Business.Enterprise.Enterprise;
import Business.Organization.Organization;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author divinity
 */
public class Manufacturer {

    private static int id = 1000;
    private String ManufacturerRegId;
    private String manufacturerName;
    private DeviceDirectory deviceDirectory;

    public Manufacturer(String name) {
        //super(EnterpriseType.Manufacturer);
        id++;
        ManufacturerRegId = "M"+id;
        deviceDirectory = new DeviceDirectory();
        this.manufacturerName = name;
        System.out.println("Manufacturer Created");

    }

    public String getManufacturerRegId() {
        return ManufacturerRegId;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public DeviceDirectory getDeviceDirectory() {
        return deviceDirectory;
    }

            @Override
        public String toString() {
            return manufacturerName;
        }

    

}
