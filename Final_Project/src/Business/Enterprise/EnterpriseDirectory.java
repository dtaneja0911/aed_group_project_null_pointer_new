/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;



import Business.HealthcareProvider.HealthcareProvider;
import Business.HealthcareProvider.HealthcareProviderDirectory;
import Business.Manufacturer.Manufacturer;
import Business.Manufacturer.ManufacturerDirectory;
import java.util.ArrayList;

/**
 *
 * @author divinity
 */
public class EnterpriseDirectory {
    
    private ArrayList<Enterprise> enterpriseList;
    
    private ManufacturerDirectory manudirectory;
    private HealthcareProviderDirectory healthcareProDir;
  

    public ArrayList<Enterprise> getEnterpriseList() {
        return enterpriseList;
    }

    public void setEnterpriseList(ArrayList<Enterprise> enterpriseList) {
        this.enterpriseList = enterpriseList;
    }
    
    public EnterpriseDirectory(){
        enterpriseList=new ArrayList<Enterprise>();
        manudirectory = new ManufacturerDirectory();
        healthcareProDir = new HealthcareProviderDirectory();
    }
    
        //create an enterprise
    
        public Enterprise createEnterprise(Enterprise.EnterpriseType type){
        Enterprise enterprise = null;
        if (type.getValue().equals(Enterprise.EnterpriseType.Manufacturer.getValue())){
            enterprise = new ManufactureEnterprise(type);
            enterpriseList.add(enterprise);
        }
        else if (type.getValue().equals(Enterprise.EnterpriseType.HealthcareProvider.getValue())){
            enterprise = new HealthcareProviderEnterprise(type);
            enterpriseList.add(enterprise);
        }
        return enterprise;
    }

    public ManufacturerDirectory getManudirectory() {
        return manudirectory;
    }

    public HealthcareProviderDirectory getHealthcareProDir() {
        return healthcareProDir;
    }
        
    
}
