/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.HealthcareProvider.HealthcareProviderDirectory;
import Business.Manufacturer.Manufacturer;
import Business.Manufacturer.ManufacturerDirectory;
import Business.Organization.Organization;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author divinity
 */
public abstract class Enterprise  extends Organization {

    private EnterpriseType enterpriseType;


    public enum EnterpriseType{

        Manufacturer("Manufacturer"),

        HealthcareProvider("Healthcare Provider");



    

        public static Object getEnterpriseType() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }



     
/*

        HealthcareProvider("HealthcareProvider");


        public static Object getEnterpriseType() {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
*/


        private String value;

        private EnterpriseType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    public EnterpriseType getEnterpriseType() {
        return enterpriseType;
    }
    
    //public abstract ArrayList<Manufacturer> getManufacturer();
    
    
    public void setEnterpriseType(EnterpriseType enterpriseType) {
        this.enterpriseType = enterpriseType;
    }

    public Enterprise(EnterpriseType type){
        super(null);
        this.enterpriseType=type;
        //manudirectory = new ManufacturerDirectory();
        //healthcareProDir = new HealthcareProviderDirectory();
        //organizationDirectory=new OrganizationDirectory();
    }


    
    
}
