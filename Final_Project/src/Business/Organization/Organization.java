/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.Role;
import Business.Staff.StaffDirectory;
import Business.UserAccount.UserAccountDirectory;


import Business.WorkQueue.WorkQueue;
import java.util.ArrayList;


/**
 *
 * @author divinity
 */
public abstract class Organization {
    
    private UserAccountDirectory userAccountdirectory;
    private StaffDirectory staffdirectory;
    private String name;


    private static int counter=0;
    private int organizationID;
    private WorkQueue workQueue;
    
        public enum Type{
        HealthAdmin("HealthAdmin Organization"), Doctor("Doctor Organization"), DeviceLab("DeviceLab Organization"), 
        Authorization("Authorization Organization"), 
        Research("Research Organization"),ManufacturerAdmin("ManufacturerAdmin Organization") ;
        private String value;
        private Type(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }

    
    public Organization(String name){
        
      userAccountdirectory = new UserAccountDirectory();  
      staffdirectory = new StaffDirectory();
      this.name = name;

       organizationID = counter;
        ++counter;
        workQueue = new WorkQueue();

    }

    public UserAccountDirectory getUserAccountdirectory() {
        return userAccountdirectory;
    }

    public StaffDirectory getStaffdirectory() {
        return staffdirectory;
    }
    

    public abstract ArrayList<Role> getSupportedRole();
    


    public int getOrganizationID() {
        return organizationID;
    }

    
    public String getName() {
        return name;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }

    @Override
    public String toString() {
        return name;
    }
    

    
    
}
