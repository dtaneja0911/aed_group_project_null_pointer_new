/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

/**
 *
 * @author divinity
 */
import Business.Country.Country;
import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.UserAccount.UserAccount;
import UserInterface.ManufacturerRole.ManufacturerAdminWorkAreaJPanel;

import javax.swing.JPanel;

public class ManufacturerAdminRole extends Role  {
    
    
        @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account,Country country, Enterprise enterprise, EcoSystem system) {
        return new ManufacturerAdminWorkAreaJPanel(userProcessContainer, enterprise);
    }
    
}
