/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Country.Country;
import Business.EcoSystem;
import Business.Enterprise.Enterprise;

import Business.Enterprise.Enterprise.EnterpriseType;


import Business.UserAccount.UserAccount;
import UserInterface.SystemAdminWorkArea.EcosystemAdminWorkAreaJPanel;

import javax.swing.JPanel;

/**
 *
 * @author raunak
 */
public class SystemAdminRole extends Role{

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account,Country country, Enterprise enterprise, EcoSystem system) {
        return new EcosystemAdminWorkAreaJPanel(userProcessContainer, system);
    }
    
}
