/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.UserAccount;

import Business.HCPEmployee.Employee;
import Business.Role.Role;
import Business.Staff.Staff;



/**
 *
 * @author raunak
 */
public class UserAccount {
    
    private String username;
    private String password;
    private Staff staff;
    private Role role;
    //private WorkQueue workQueue;

    /*public UserAccount() {
        workQueue = new WorkQueue();
    }*/
    
     
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }


    public void setRole(Role role) {
        this.role = role;
    }

    public Staff getStaff() {
        return staff;
    }

    public void setStaff(Staff staff) {
        this.staff = staff;
    }



    /*public WorkQueue getWorkQueue() {
        return workQueue;
    }*/

    
    
    @Override
    public String toString() {
        return username;
    }
    
    
    
}