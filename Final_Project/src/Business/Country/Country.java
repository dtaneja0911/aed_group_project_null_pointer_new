/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Country;

import Business.Enterprise.EnterpriseDirectory;


/**
 *
 * @author divinity
 */
public class Country {
    private EnterpriseDirectory enterpriseDir;
   // private ManufacturerDirectory manuDirectory;
   // private HealthcareProviderDirectory healthcareProDir;
    private String countryName;
    private String countryCode;
    private static int count = 100;

    public Country() {
        count++;
        countryCode = "CNT" + count;
        enterpriseDir = new EnterpriseDirectory();
        //healthcareProDir = new HealthcareProviderDirectory();

    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public EnterpriseDirectory getEnterpriseDir() {
        return enterpriseDir;
    }

    public String getCountryCode() {
        return countryCode;
    }

        @Override
    public String toString() {
        return this.getCountryName();
    }

}
