/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Department;

import Business.HCPEmployee.Employee;
import Business.HealthcareProvider.HealthcareProvider;

/**
 *
 * @author divinity
 */
public class Department {
    
    private String depId;
    private String depName;
    private Employee employee;
    private HealthcareProvider hcp;
    private int id;
    
    public Department(HealthcareProvider hcp, Employee emp)
    {
        this.hcp = hcp;
        id++;
        depId = "Dep"+ id;
        this.employee = emp;
       
    }

    public String getDepName() {
        return depName;
    }

    public void setDepName(String depName) {
        this.depName = depName;
    }

    public Employee getEmployee() {
        return employee;
    }


    public HealthcareProvider getHcp() {
        return hcp;
    }

    public String getDepId() {
        return depId;
    }
    @Override
    public String toString(){
        return depName;
    }   
}
