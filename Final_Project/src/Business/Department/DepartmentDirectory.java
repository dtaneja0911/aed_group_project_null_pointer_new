/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Department;

import Business.HCPEmployee.Employee;
import Business.HealthcareProvider.HealthcareProvider;
import java.util.ArrayList;

/**
 *
 * @author divinity
 */
public class DepartmentDirectory {

    private ArrayList<Department> departmentList;

    public DepartmentDirectory() {
        departmentList = new ArrayList<>();
        initializeDepartnment();
    }

    public ArrayList<Department> getCategory() {
        return departmentList;
    }

    public void initializeDepartnment() {

        Department dep1 = new Department(null, null);
        String depName1 = "Neurology";
        dep1.setDepName(depName1);
        departmentList.add(dep1);

        Department dep2 = new Department(null, null);
        String depName2 = "Cardiology";
        dep1.setDepName(depName2);
        departmentList.add(dep2);

        Department dep3 = new Department(null, null);
        String depName3 = "Gynecology";
        dep1.setDepName(depName3);
        departmentList.add(dep3);

        Department dep4 = new Department(null, null);
        String depName4 = "Orthopediatric";
        dep1.setDepName(depName4);
        departmentList.add(dep4);
    }

    public Department createAndAddDepartment(HealthcareProvider hcp, Employee emp) {
        Department dep = new Department(hcp, emp);
        departmentList.add(dep);
        return dep;
    }

}
