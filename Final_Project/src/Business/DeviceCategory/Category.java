
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.DeviceCategory;

/**
 *
 * @author divinity
 */
public class Category {

    private String categoryId;
    private String categoryName;
    private int cat;

    public Category() {
        cat++;
        categoryId = "Category" + cat;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
    
    

}
