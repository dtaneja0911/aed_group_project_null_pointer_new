/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.DeviceCategory;

import java.util.ArrayList;

/**
 *
 * @author divinity
 */
public class CategoryDirectory {
    
    private ArrayList<Category> categoryList;
    
    public CategoryDirectory()
    {
        categoryList = new ArrayList<>();
    }

    public ArrayList<Category> getCategory() {
        return categoryList;
    }
    
     public Category createAndAddCategory() {
        Category cat = new Category();
        categoryList.add(cat);
        return cat;
    }
    
}
