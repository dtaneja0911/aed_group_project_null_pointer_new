/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.DeviceCategory;

import Business.Manufacturer.Manufacturer;
import java.util.ArrayList;

/**
 *
 * @author divinity
 */
public class DeviceDirectory {
    
        private ArrayList<Device> deviceList;

    public DeviceDirectory() {
        deviceList = new ArrayList<>();

    }

    public ArrayList<Device> getHealthcareProList() {
        return deviceList;
    }

    public Device createAndAddDevice(Manufacturer manu, Category cat) {
        Device dev = new Device(manu,cat);
        deviceList.add(dev);
        return dev;
    }
    
}
