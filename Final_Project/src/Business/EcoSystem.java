/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Country.Country;
import Business.Organization.Organization;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author divinity
 */
public class EcoSystem extends Organization{

    private static EcoSystem business;
    private ArrayList<Country> countryList;
    
    private EcoSystem(){
        super(null);
        countryList=new ArrayList<>();
    }


    public static EcoSystem getInstance() {
        System.out.println("ecosystem start");
        if (business == null) {
            business = new EcoSystem();
        }
        return business;
    }

    public Country createAndAddCountry() {
        Country country = new Country();
        countryList.add(country);
        return country;
    }

    public ArrayList<Country> getCountryList() {
        return countryList;
    }

    public void setCountryList(ArrayList<Country> countryList) {
        this.countryList = countryList;
    }
    
       
   /* public boolean checkIfUserIsUnique(String userName){
        if(!this.getUserAccountDirectory().checkIfUsernameIsUnique(userName)){
            return false;
        }
        for(Network network:networkList){
            
        }
        return true;
    }*/

    @Override
    public ArrayList<Role> getSupportedRole() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    

}
