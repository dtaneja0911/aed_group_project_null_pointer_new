/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.HealthcareProvider;

import java.util.ArrayList;

/**
 *
 * @author divinity
 */
public class HealthcareProviderDirectory {

    private ArrayList<HealthcareProvider> healthcareProList;

    public HealthcareProviderDirectory() {
        healthcareProList = new ArrayList<>();

    }

    public ArrayList<HealthcareProvider> getHealthcareProList() {
        return healthcareProList;
    }

    public HealthcareProvider createAndAddHealthCarePro(String name) {
        HealthcareProvider healthcp = new HealthcareProvider(name);
        healthcareProList.add(healthcp);
        return healthcp;
    }

}
