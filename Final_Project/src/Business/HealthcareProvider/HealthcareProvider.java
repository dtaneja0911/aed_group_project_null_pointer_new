/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.HealthcareProvider;

import Business.Department.DepartmentDirectory;
import Business.Enterprise.Enterprise;
import Business.Manufacturer.Manufacturer;
import Business.Organization.Organization;
import java.util.ArrayList;

/**
 *
 * @author divinity
 */
public class HealthcareProvider {

    private static int id = 1000;
    private String healthcareRegId;
    private String hospitalName;
  
    private DepartmentDirectory departmentDirectory;
    

    public HealthcareProvider(String name) {
        //super(name,EnterpriseType.HealthcareProvider);
        id++;
        healthcareRegId = "HCP" + id;
        departmentDirectory = new DepartmentDirectory();
        
        this.hospitalName = name;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public DepartmentDirectory getDepartmentDirectory() {
        return departmentDirectory;
    }

    public String getHealthcareRegId() {
        return healthcareRegId;
    }


}
