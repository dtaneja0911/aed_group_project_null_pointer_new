/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterfaces;

/**
 *
 * @author User
 */
public class HealthcareAdminWorkAreaJPanel extends javax.swing.JPanel {

    /**
     * Creates new form HealthcareAdminWorkAreaJPanel
     */
    public HealthcareAdminWorkAreaJPanel() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        lblEnterprise = new javax.swing.JLabel();
        lblEnterpriseName = new javax.swing.JLabel();
        btnManageDepartments = new javax.swing.JButton();
        btnManageEmployees = new javax.swing.JButton();
        btnManageEmployeeAccess = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("My Work Area - Healthcare Organization Admin");

        lblEnterprise.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblEnterprise.setText("Enterprise: ");

        lblEnterpriseName.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lblEnterpriseName.setText("<Enterprise Name e.g. HealthcareProvider\"A\">");

        btnManageDepartments.setText("Manage Departments");

        btnManageEmployees.setText("Manage Employees");

        btnManageEmployeeAccess.setText("Manage Employee Login Access");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(74, 74, 74)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblEnterprise)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnManageDepartments)
                            .addComponent(lblEnterpriseName)
                            .addComponent(btnManageEmployees)
                            .addComponent(btnManageEmployeeAccess))))
                .addContainerGap(180, Short.MAX_VALUE))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnManageDepartments, btnManageEmployeeAccess, btnManageEmployees});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jLabel1)
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblEnterprise)
                    .addComponent(lblEnterpriseName))
                .addGap(18, 18, 18)
                .addComponent(btnManageDepartments)
                .addGap(18, 18, 18)
                .addComponent(btnManageEmployees)
                .addGap(18, 18, 18)
                .addComponent(btnManageEmployeeAccess)
                .addContainerGap(187, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnManageDepartments;
    private javax.swing.JButton btnManageEmployeeAccess;
    private javax.swing.JButton btnManageEmployees;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel lblEnterprise;
    private javax.swing.JLabel lblEnterpriseName;
    // End of variables declaration//GEN-END:variables
}
