/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4;

import assignment_4.analytics.AnalysisHelper;
import assignment_4.analytics.DataStore;
import assignment_4.entities.Customer;
import assignment_4.entities.Item;
import assignment_4.entities.Order;
import assignment_4.entities.Product;
import assignment_4.entities.SalesPerson;
import java.io.IOException;


/**
 *
 * @author divinity
 */
public class Assignment4Main {

    DataReader salesDataReader;
    DataReader productCatalogueReader;
    AnalysisHelper helper;

    public Assignment4Main() throws IOException {
        DataGenerator generator = DataGenerator.getInstance();
        salesDataReader = new DataReader(generator.getOrderFilePath());
        productCatalogueReader = new DataReader(generator.getProductCataloguePath());
        helper = new AnalysisHelper();
    }

    public static void main(String[] args) throws IOException {
        Assignment4Main inst = new Assignment4Main();
        inst.readData();
    }

    private void readData() throws IOException {
        String[] row;
        while ((row = productCatalogueReader.getNextRow()) != null) {
            //System.out.println("inside product file put to map");
            generateProductCatMap(row);
            //System.out.println("after product file put to map");
        }
        while ((row = salesDataReader.getNextRow()) != null) {
            //System.out.println("inside sales data file putting to maps");
            Item item = getItems(row);
            getCustomerdata(row);
            getSalesPersonDtl(row);
            getOrderDetails(row, item);

        }

        runAnalysis();
    }

    private void generateProductCatMap(String[] row) {
        int ProductId = Integer.parseInt(row[0]);
        int minPrc = Integer.parseInt(row[1]);
        int maxPrc = Integer.parseInt(row[2]);
        int targetPrc = Integer.parseInt(row[3]);

        Product p = new Product(ProductId, minPrc, maxPrc, targetPrc);
        DataStore.getInstance().getProduct().put(ProductId, p);
        //System.out.println("inside product class map");
    }

    private Item getItems(String[] row) {
        int productId = Integer.parseInt(row[2]);
        int salesPrice = Integer.parseInt(row[6]);
        int quantity = Integer.parseInt(row[3]);
        Item item = new Item(productId, salesPrice, quantity);

        DataStore.getInstance().getItem().put(productId, item);
        //System.out.println("inside Item class map");
        return item;

    }

    private void getCustomerdata(String[] row) {

        int custId = Integer.parseInt(row[5]);
        Customer cust = new Customer(custId);
        DataStore.getInstance().getCustomer().put(custId, cust);
         //System.out.println("inside Customer class map");

    }

    private void getSalesPersonDtl(String[] row) {

        int salesPersonId = Integer.parseInt(row[4]);
        SalesPerson sales = new SalesPerson(salesPersonId);
        DataStore.getInstance().getSalesPerson().put(salesPersonId, sales);
        //System.out.println("inside salesperson class map");

    }

    private void getOrderDetails(String[] row, Item item) {

        int orderId = Integer.parseInt(row[0]);
        int salesId = Integer.parseInt(row[4]);
        int customerId = Integer.parseInt(row[5]);
        Order order = new Order(orderId, salesId, customerId, item);
        DataStore.getInstance().getOrder().put(orderId, order);
     //System.out.println("inside order class map");

    }

    private void runAnalysis() throws IOException {

        helper.ThreeMostPopularProducts();
        helper.ThreeBestSalesPeople();
        helper.threeBestCustomers();
        helper.totalRevenueForTheYear();
        //printall();
    }
    
    
    
    public static void printall () throws IOException{
        
        DataGenerator generator = DataGenerator.getInstance();
        
        //Below is the sample for how you can use reader. you might wanna 
        //delete it once you understood.
        
        DataReader orderReader = new DataReader(generator.getOrderFilePath());
        String[] orderRow;
        printRow(orderReader.getFileHeader());
        while((orderRow = orderReader.getNextRow()) != null){
            printRow(orderRow);
        }
        System.out.println("_____________________________________________________________");
        DataReader productReader = new DataReader(generator.getProductCataloguePath());
        String[] prodRow;
        printRow(productReader.getFileHeader());
        while((prodRow = productReader.getNextRow()) != null){
            printRow(prodRow);
        }
    }
    
        public static void printRow(String[] row){
        for (String row1 : row) {
            System.out.print(row1 + ", ");
        }
        System.out.println("");
    }

}
