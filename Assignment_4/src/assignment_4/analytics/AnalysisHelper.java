/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4.analytics;

import assignment_4.entities.Customer;
import assignment_4.entities.Item;
import assignment_4.entities.Order;
import assignment_4.entities.Product;
import assignment_4.entities.SalesPerson;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author divinity
 */
public class AnalysisHelper {

    public void ThreeMostPopularProducts() {
        Map<Integer, Integer> productQtyCountInOrders = new HashMap<>();
        Map<Integer, Product> product = DataStore.getInstance().getProduct();
        Map<Integer, Order> order = DataStore.getInstance().getOrder();
        Map<Integer, Item> item = DataStore.getInstance().getItem();

        for (Product p : product.values()){
            int qty_count = 0;
            for (Order o : order.values()){
                int prod = o.getItem().getProductId();
                if (prod == p.getProductId()){
                    qty_count += o.getItem().getQuantity();
                }
            }
            productQtyCountInOrders.put(p.getProductId(), qty_count);
        }
        //System.out.println(productQtyCountInOrders);
        
        List<Entry<Integer,Integer>> popularProducts = new ArrayList<>(productQtyCountInOrders.entrySet());
        Collections.sort(popularProducts, new Comparator<Entry<Integer,Integer>>(){
            @Override
            public int compare(Entry<Integer, Integer> o1, Entry<Integer, Integer> o2) {
                return o2.getValue().compareTo(o1.getValue());
            }
        });
        System.out.println("");
        System.out.println("***********Top 3 Best Products***********\n");
        for (int i=0; i<3; i++){
            Entry entry = popularProducts.get(i);
            System.out.println("ProductId: "+entry.getKey()+" Quantity Sold: "+entry.getValue());
        }
        System.out.println("-----------------------------------------------------------------------");
    }

    public void ThreeBestSalesPeople() {
        //Best salses persons are who generated most revenue 
        //Revenue per order = revenue per product (selling price - min value) * quantity sold
        //Totalsalerevenue = sum of all the order_revenue by a salesperson
        //take out the top three

        Map<Integer, SalesPerson> salesperson = DataStore.getInstance().getSalesPerson();
        Map<Integer, Order> orderdtl = DataStore.getInstance().getOrder();
        Map<Integer, Product> productbySalesperson = DataStore.getInstance().getProduct();
        Map<Integer, Integer> TotalSalebySalesPerson = new HashMap<>();

        for(SalesPerson sp : salesperson.values()) {
            int Totalsalerevenue = 0;
            for(Order o : orderdtl.values()) {
                int productrevenue = 0;
                if(o.getSalesId() == sp.getSalesId()) {
                    int product = o.getItem().getProductId();//5
                    for(Product p : productbySalesperson.values()) {
                        if(product == p.getProductId()) {
                            int saleprice = o.getItem().getSalesPrice();
                            int minprice = p.getMinPrice();
                            int revenue = saleprice - minprice;
                            int quantity = o.getItem().getQuantity();
                            productrevenue = revenue * quantity;
                        }
                    }
                }
                Totalsalerevenue += productrevenue;
            }
            TotalSalebySalesPerson.put(sp.getSalesId(), Totalsalerevenue);
            //System.out.println("salesperson with Id " + sp.getSalesId() + " has total sales of " + Totalsalerevenue);

        }
        List<Entry<Integer, Integer>> entrySalesRevenue= new ArrayList<>(TotalSalebySalesPerson.entrySet());
        Collections.sort(entrySalesRevenue, new  Comparator<Entry<Integer, Integer>>(){
            @Override
            public int compare(Entry<Integer, Integer> e1, Entry<Integer, Integer> e2) {
                 return e2.getValue().compareTo(e1.getValue());
            }
        });
        System.out.println("");
        System.out.println("********** Top 3 best Salesperson ********\n");
        for(int i=0; i<3; i++){
            Entry entry= entrySalesRevenue.get(i);
            System.out.println("The salesman with id:"+ entry.getKey()+ " "+ "generates the total revenue:"+ entry.getValue());
        }
        System.out.println("-----------------------------------------------------------------------");
    }
    
    public void threeBestCustomers(){
        System.out.println("-----------------------------------------------------------------------");
        //System.out.println("Customers with their total purchase:");
        Map<Integer, Customer> customers= DataStore.getInstance().getCustomer();
        Map<Integer, Order> orders= DataStore.getInstance().getOrder();
        Map<Integer, Product> products= DataStore.getInstance().getProduct();
        Map<Integer, Integer> customerRevenue= new HashMap<>();
        for(Customer cust: customers.values()){
            int revenuePerCustomer= 0;
            for(Order order: orders.values()){
                if(order.getCustomerId()== cust.getCustomerId()){
                    for(Product product: products.values()){
                        if(product.getProductId()== order.getItem().getProductId()){
                            int revenuePerProduct= order.getItem().getSalesPrice()- product.getMinPrice();
                            int totalRevenePerOrder= order.getItem().getQuantity()* revenuePerProduct;
                            revenuePerCustomer= revenuePerCustomer+ totalRevenePerOrder;
                        }
                    }
                }
            }
            customerRevenue.put(cust.getCustomerId(), revenuePerCustomer);
            //System.out.println("Customer with Id " + cust.getCustomerId() + " generates the revenue:" + revenuePerCustomer);
        }
        List<Entry<Integer, Integer>> entryCustRevenue= new ArrayList<Entry<Integer, Integer>>(customerRevenue.entrySet());
        Collections.sort(entryCustRevenue, new  Comparator<Entry<Integer, Integer>>(){
            @Override
            public int compare(Entry<Integer, Integer> e1, Entry<Integer, Integer> e2) {
                 return e2.getValue().compareTo(e1.getValue());
            }
        });
        System.out.println("********** Top 3 best Customer ********\n");
        for(int i=0; i<3; i++){
            Entry entry= entryCustRevenue.get(i);
            System.out.println("The customer with id:"+ entry.getKey()+ " "+ "generates the revenue:"+ entry.getValue());
        }
        System.out.println("-----------------------------------------------------------------------");
    }
    
    public void totalRevenueForTheYear(){
        System.out.println("-----------------------------------------------------------------------");
        Map<Integer, Order> orders= DataStore.getInstance().getOrder();
        Map<Integer, Product> products= DataStore.getInstance().getProduct();
        int totalRevenue= 0;
        for(Order order: orders.values()){
            for(Product product: products.values()){
                if(order.getItem().getProductId()== product.getProductId()){
                    int revenuePerItem= order.getItem().getSalesPrice()-product.getMinPrice();
                    int revenuePerOrder= revenuePerItem* order.getItem().getQuantity();
                    totalRevenue= totalRevenue+ revenuePerOrder;
                }
            }
        }
        System.out.println("********** Total Revenue of the Year ********\n");
        System.out.println("Our total revenue for the year:"+ totalRevenue);
        System.out.println("-----------------------------------------------------------------------");
    }
}
